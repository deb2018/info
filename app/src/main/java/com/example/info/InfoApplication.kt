package com.example.info

import android.app.Application
import com.example.info.network.Api
import com.example.info.network.NetworkConnectionInterceptor
import com.example.info.repository.InfoRepository
import com.example.info.ui.InfoViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class InfoApplication : Application(), KodeinAware {

    override val kodein = Kodein.lazy {
        import(androidXModule(this@InfoApplication))

        bind() from singleton { NetworkConnectionInterceptor(instance()) }
        bind() from singleton { Api(instance()) }
        bind() from singleton { InfoRepository(instance()) }
        bind() from provider { InfoViewModelFactory(instance()) }

    }
}