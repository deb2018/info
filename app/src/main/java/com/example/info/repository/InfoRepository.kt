package com.example.info.repository

import com.example.info.model.InfoResponse
import com.example.info.network.Api
import com.example.info.network.SafeApiRequest


class InfoRepository(
    private val api: Api
) : SafeApiRequest() {

    suspend fun getInfoData() : InfoResponse {

        return apiRequest { api.infoList() }
    }
}