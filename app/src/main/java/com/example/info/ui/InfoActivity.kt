package com.example.info.ui

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.info.R
import com.example.info.model.InfoResponse
import com.example.info.utils.hide
import com.example.info.utils.show
import com.example.info.utils.snackBar
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_info.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class InfoActivity : AppCompatActivity(), InfoDataListener, KodeinAware {

    override val kodein by kodein()
    private val feedViewModelFactory : InfoViewModelFactory by instance()
    private lateinit var swipeRefreshLayout : SwipeRefreshLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_info)
        val viewModel = ViewModelProvider(this,feedViewModelFactory).get(InfoViewModel::class.java)
        viewModel.infoDataListener = this
        viewModel.getInfoData()

        swipeRefreshLayout = findViewById(R.id.swipe_lay);
        swipeRefreshLayout.setOnRefreshListener { viewModel.getInfoData() }
    }


    override fun onStarted() {
        pbHolder.show()
    }

    override fun onSuccess(infoResponse: InfoResponse) {
        supportActionBar?.title = infoResponse.title
        pbHolder.hide()
        if (swipeRefreshLayout.isRefreshing)
            swipeRefreshLayout.isRefreshing = false
        rvInfoList.visibility = View.VISIBLE
        initRecyclerView(infoResponse.rows.toInfoItem())
    }

    override fun onFailure(errorMsg: String) {
        pbHolder.hide()
        info_root.snackBar(errorMsg)
        if (swipeRefreshLayout.isRefreshing)
            swipeRefreshLayout.isRefreshing = false
        rvInfoList.visibility = View.GONE
    }


    private fun initRecyclerView(feedItem: List<InfoItem>) {

        val feedAdapter = GroupAdapter<ViewHolder>().apply {
            addAll(feedItem)
        }

        rvInfoList.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = feedAdapter
        }
    }

    private fun List<InfoResponse.Data>.toInfoItem() : List<InfoItem>{
        return this.map {
            InfoItem(it)
        }
    }
}