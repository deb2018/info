package com.example.info.ui


import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.info.R
import com.example.info.databinding.ItemInfoBinding
import com.example.info.model.InfoResponse
import com.xwray.groupie.databinding.BindableItem

class InfoItem(
    private val infoItem: InfoResponse.Data
) : BindableItem<ItemInfoBinding>(){

    override fun getLayout() = R.layout.item_info

    override fun bind(viewBinding: ItemInfoBinding, position: Int) {
        viewBinding.info = infoItem
        Glide
            .with(viewBinding.tvTitle.context)
            .load(infoItem.imageHref)
            .into(viewBinding.ivImage)
    }

}