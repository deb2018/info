package com.example.info.ui

import androidx.lifecycle.ViewModel
import com.example.info.repository.InfoRepository
import com.example.info.utils.ApiException
import com.example.info.utils.Coroutines
import com.example.info.utils.NoInternetException


class InfoViewModel(
    private val infoRepository: InfoRepository
) : ViewModel() {

    var infoDataListener: InfoDataListener? = null


    fun getInfoData(){
        infoDataListener?.onStarted()

        Coroutines.main {
            try {
                val infoResponse = infoRepository.getInfoData()
                infoDataListener?.onSuccess(infoResponse)
            }catch (e: ApiException){
                infoDataListener?.onFailure(e.message!!)
            }catch (e: NoInternetException){
                infoDataListener?.onFailure(e.message!!)
            }
        }

    }
}
