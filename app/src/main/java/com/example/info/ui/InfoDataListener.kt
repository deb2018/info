package com.example.info.ui

import com.example.info.model.InfoResponse


interface InfoDataListener {

    fun onStarted()
    fun onSuccess(infoResponse: InfoResponse)
    fun onFailure(errorMsg: String)
}