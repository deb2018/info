package com.example.info.model

import kotlin.collections.ArrayList


data class InfoResponse (
    val title: String,
    val rows: ArrayList<Data>

) {
    data class Data (
        val title: String,
        val description: String,
        val imageHref: String
    )

}