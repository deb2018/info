package com.example.info.ui


import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.example.info.R
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@LargeTest
@RunWith(AndroidJUnit4::class)
class InfoActivityTest{


    @Rule
    @JvmField
    var activityRule = ActivityScenarioRule(InfoActivity::class.java)

    lateinit var recyclerView : RecyclerView
     var itemCount : Int = 0

    @Before
    fun setUp(){

        Thread.sleep(2000)
        activityRule.scenario.onActivity {
            recyclerView = it.findViewById(R.id.rvInfoList)
            itemCount = recyclerView.adapter?.itemCount!!

        }
    }

    @Test
    fun testUI(){

        if(itemCount > 0)
            onView(withId(R.id.rvInfoList)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(itemCount.minus(1)))

    }


}